# OpenGL-3D天空迷宫

#### Description
3D天空迷宫游戏。
使用C++ OpenGL 3.3，编辑器采用Qt 5.9.1及VS2015。使用天空盒、纹理贴图、摄像机等技术，本视频对软件的安装，代码使用，打包等问题进行了讲解。适合学习C++，并学习一段时间OpenGL的童鞋食用。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)